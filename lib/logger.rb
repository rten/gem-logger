require "logger/version"

module Log
	@DEBUG_MODE = false
	class UnimplementedCode < StandardError
		def initialize(msg="Unimplemented code")
			super
		end
	end

	def self.me(message, args = {})
	 	# Logs 
		puts "LOG:#{(args[:sender] + ":") if args[:sender]}#{(args[:line].to_s + ":") if args[:line]} #{message}"	if @DEBUG_MODE
	end

	def self.todo(*args)
		raise UnimplementedCode, "Unimplemented code #{": " + args.join if args}" if @DEBUG_MODE
	end

	def self.set_debug(bool)
		bool ? @DEBUG_MODE = true : @DEBUG_MODE = false
	end
end